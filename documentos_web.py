import urllib.request


class Robot:
    def __init__(self, url):
        self.url = url
        self._content = None

    def retrieve(self):  # Se encarga de descargar la url de la que se ocupa el robot
        if self._content is None:
            print(f"Descargando {self.url}") #Para mostrar la url que estas descargando damos formato al print
            w = urllib.request.urlopen(self.url)
            self._content = w.read().decode('utf-8')

    def show(self): #mostrarña en pantalla el contenido descargado
        self.retrieve()
        print(self._content)

    def content(self): #devolverá una cadena de caracteres string
        if not self._content:
            self.retrieve()
        return self._content


class Cache:
    def __init__(self):
        self.cache = {}

    def retrieve(self, url): #descargar el documento correspondiente a esa url
        if url not in self.cache:
            robot = Robot(url)
            robot.retrieve()
            self.cache[url] = robot.content()

    def show(self,url): #mostrará por pantalla el contenido del documento
        self.retrieve(url)
        print(self.cache[url])

    def show_all(self): #mostrará un listado de todas las urls
        for url in self.cache:
            print(url)

    def content(self,url): #devuelve la cadena de caracteres con el contenido del documento
        if url not in self.cache:
            self.retrieve(url)
        return self.cache[url]

if __name__ == '__main__':
    print("*********ROBOT*********")
    #Crear dos objetos de la clase robot
    robot1 = Robot('http://gsyc.urjc.es/')
    robot2 = Robot('https://www.aulavirtual.urjc.es')

    #Llamar a los metodos de la clase robot
    robot1.retrieve()
    robot1.show()

    robot2.retrieve()
    robot2.show()

    #Crear objeto de la clase cache
    cache = Cache()

    #Llamar  a los metodos de los objetos de la clase cache
    cache.retrieve('http://gsyc.urjc.es/')
    cache.retrieve('https://www.aulavirtual.urjc.es')

    cache.show('http://gsyc.urjc.es/')
    cache.show('https://www.aulavirtual.urjc.es')

    cache.show_all()
